package com.stylingandroid.Animation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class FirstActivity extends Activity
{
	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		setContentView( R.layout.first );
		((Button)findViewById( R.id.NextButton )).setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				startActivity( new Intent( FirstActivity.this, SecondActivity.class ) );
				overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
			}
		});
	}
}
