/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.devision.intent.explicit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;

/**
 *
 * @author mt
 */
public class ActivityTwo extends Activity {
 
	Button button;
 
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.second);
                addListenerOnButton();
	}
        
        public void addListenerOnButton() {
 
		final Context context = this;
 
		button = (Button) findViewById(R.id.button2);
 
		button.setOnClickListener(new View.OnClickListener() {
 
			@Override
			public void onClick(View arg0) {
 
			    finish(); 
 
			}
 
		});
 
	}
 
}