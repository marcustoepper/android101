package com.collabera.labs.sai;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class BooksActivity extends ListActivity {

		
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, BOOKS));
        getListView().setTextFilterEnabled(true);
        
    }
    
    static final String[] BOOKS = new String[]{
    	"The Busy Coder's Guide to Android Development",
    	"Android Essentials",
    	"Android - Programmer's Guide",
    	"Professional Android Application Development",
    	"Unlocking Android",
    	"Android Application Development",
    	"Pro Android",
    	"Beginning Android",
    	"Android Programming Tutorials"
    };
    protected void onListItemClick(ListView l, View v, int position, long id) {
    	super.onListItemClick(l, v, position, id);
    	Object o = this.getListAdapter().getItem(position);
    	String book = o.toString();
    	Intent returnIntent = new Intent();
    	returnIntent.putExtra("SelectedBook",book);
    	setResult(RESULT_OK,returnIntent);    	
    	finish();
    }
}
