package com.collabera.labs.sai;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class PensActivity extends ListActivity {

	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, PENS));
        getListView().setTextFilterEnabled(true);
        
    }
    
    static final String[] PENS = new String[]{
    	"MONT Blanc",
    	"Gucci",
    	"Parker",
    	"Sailor",
    	"Porsche Design",
    	"Rotring",
    	"Sheaffer",
    	"Waterman"
    };
    protected void onListItemClick(ListView l, View v, int position, long id) {
    	super.onListItemClick(l, v, position, id);
    	Object o = this.getListAdapter().getItem(position);
    	String pen = o.toString();
    	Intent returnIntent = new Intent();
    	returnIntent.putExtra("SelectedPen",pen);
    	setResult(RESULT_OK,returnIntent);    	
    	finish();
    }

}
