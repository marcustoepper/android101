package com.collabera.labs.sai;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class CallingActivity extends Activity {

	public static final int BOOK_SELECT = 1;
	public static final int PEN_SELECT = 2;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.callingactivity);
        
        try {
            Button books = (Button)findViewById(R.id.SelectBooks);
            books.setOnClickListener(new OnClickListener() {
            	
            	public void onClick(View v){
            		Intent bookIntent = new Intent();
            		bookIntent.setClass(CallingActivity.this,BooksActivity.class);
            		startActivityForResult(bookIntent,BOOK_SELECT);
            	}
            });
        } catch (ActivityNotFoundException anfe) {
        	Log.e("onCreate", "Book Activity Not Found", anfe);
        }
        
        
        try {
            Button pens = (Button)findViewById(R.id.SelectPens);
            pens.setOnClickListener(new OnClickListener() {
            	
            	public void onClick(View v){
            		Intent penIntent = new Intent();
            		penIntent.setClass(CallingActivity.this,PensActivity.class);
            		startActivityForResult(penIntent,PEN_SELECT);
            	}
            });
        } catch (ActivityNotFoundException anfe) {
        	Log.e("onCreate", "Pen Activity Not Found", anfe);
        }
    }

    protected void start(Intent intent) {
    	this.startActivityForResult(intent,BOOK_SELECT);
    }
    
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
    	switch(requestCode) {
    	case BOOK_SELECT: 
            if (resultCode == RESULT_OK) {
                String name = data.getStringExtra("SelectedBook");
                Toast.makeText(this, "You have chosen the book: " + " " + name, Toast.LENGTH_LONG).show();
                break;
            }
    	case PEN_SELECT:
            if (resultCode == RESULT_OK) {
                String name = data.getStringExtra("SelectedPen");
                Toast.makeText(this, "You have chosen the pen: " + " " + name, Toast.LENGTH_LONG).show();
                break;
            }
    	}
 
	}   

}