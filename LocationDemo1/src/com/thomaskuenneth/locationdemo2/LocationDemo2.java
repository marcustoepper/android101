package com.thomaskuenneth.locationdemo2;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;

public class LocationDemo2 extends MapActivity {

	private static final String TAG = LocationDemo2.class.getSimpleName();

	private LocationManager manager;
	private LocationListener listener;
	private MapController mapController;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		// Location Manager ermitteln
		manager = (LocationManager) getSystemService(LOCATION_SERVICE);
		// LocationListener definieren
		listener = new LocationListener() {
			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
				Log.d(TAG, "onStatusChanged()");
			}

			@Override
			public void onProviderEnabled(String provider) {
				Log.d(TAG, "onProviderEnabled()");
			}

			@Override
			public void onProviderDisabled(String provider) {
				Log.d(TAG, "onProviderDisabled()");
			}

			@Override
			public void onLocationChanged(Location location) {
				Log.d(TAG, "onLocationChanged()");
				// Koordinaten umwandeln
				int lat = (int) (location.getLatitude() * 1E6);
				int lng = (int) (location.getLongitude() * 1E6);
				GeoPoint point = new GeoPoint(lat, lng);
				mapController.setCenter(point);
			}
		};
		// Zoom aktivieren
		MapView mapView = (MapView) findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(true);
		// MapController ermitteln
		mapController = mapView.getController();
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.d(TAG, "onStart()");
		manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 0,
				listener);
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause()");
		manager.removeUpdates(listener);
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
}
