package de.devision.sqlite.first;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

//import android.widget.Switch;
import android.widget.Toast;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.CompoundButton;

import de.devision.sqlite.first.R;





public class SecondScreen extends Activity implements SeekBar.OnSeekBarChangeListener, CompoundButton.OnCheckedChangeListener {
	
	SeekBar mySeekBar;
	TextView myProgressText;

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second);
     
        mySeekBar = (SeekBar)findViewById(R.id.seekBar1);
        mySeekBar.setOnSeekBarChangeListener(this);
        mySeekBar.setEnabled(false);
        
        myProgressText = (TextView)findViewById(R.id.textView3);
        
        //Switch mySwitch = (Switch) findViewById(R.id.switch1);
        //mySwitch.setOnCheckedChangeListener(this);
        //mySwitch.setChecked(false);
        
    }
    
    
    public void dismissView (View view) {
    	
    	finish();
    }
    
    public void showAlert (View view) {
    	
    	Context context = getApplicationContext();
    	CharSequence text = "Hi, im a Toast Alert";
    	int duration = Toast.LENGTH_SHORT;

    	Toast toast = Toast.makeText(context, text, duration);
    	toast.show();
    	
    }
    
    // inherent Seek Bar Methods
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
        myProgressText.setText(progress + "");
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
        
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
        
    }

    // inherent Switch Methods
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        
    	if (isChecked) {
			mySeekBar.setEnabled(true);
		} else {
			mySeekBar.setEnabled(false);
			
		}
     }

    


}

