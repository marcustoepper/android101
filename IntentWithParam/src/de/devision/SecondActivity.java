/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.devision;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

/**
 *
 * @author mt
 */
public class SecondActivity extends Activity {

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.second); 
        
        Bundle b = getIntent().getExtras();
        int id = b.getInt("myParam");
        String bla = b.getString("stringparam");
        
        System.out.println("hallo " + id + bla);
        Toast.makeText(this, "hallo " + id + bla, Toast.LENGTH_LONG).show();
    }
}
